<?php

namespace KDA\Laravel\MailHistory;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\MailHistory\Facades\MailHistory as Facade;
use KDA\Laravel\MailHistory\MailHistory as Library;
use KDA\Laravel\Traits\HasLoadableMigration;
use KDA\Laravel\Traits\HasProviders;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasLoadableMigration;
    use HasProviders;
    protected $packageName = 'laravel-mail-history';
    protected $additionnalProviders = [
        EventServiceProvider::class
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }
    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
