<?php

namespace KDA\Laravel\MailHistory\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class MailLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'from',
        'to',
        'cc',
        'bcc',
        'subject',
        'html_body',
        'text_body',
        'raw_body',
        'sent_debug_info',
    ];

    protected $appends = [];

    protected $casts = [];


    protected static function newFactory()
    {
        return  \KDA\Laravel\MailHistory\Database\Factories\MailLogFactory::new();
    }

    /*
    public function getTable()
    {
     
    }
*/
}
