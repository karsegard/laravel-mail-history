<?php

namespace KDA\Laravel\MailHistory\Facades;

use Illuminate\Support\Facades\Facade;

class MailHistory extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
