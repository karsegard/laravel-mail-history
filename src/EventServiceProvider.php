<?php

namespace KDA\Laravel\MailHistory;


use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Support\Facades\Event;
use KDA\Laravel\MailHistory\Listeners\EmailListener;

class EventServiceProvider extends ServiceProvider
{


    public function boot()
    {
        parent::boot();
        Event::listen(MessageSent::class, EmailListener::class);
    }
}
