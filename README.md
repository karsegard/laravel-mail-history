# This is my package laravel-mail-history

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/laravel-mail-history.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-mail-history)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/laravel-mail-history.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-mail-history)

## Installation

You can install the package via composer:

```bash
composer require kda/laravel-mail-history
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="\KDA\Laravel\MailHistory\ServiceProvider" --tag="migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --provider="\KDA\Laravel\MailHistory\ServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
return [
];
```

Optionally, you can publish the views using

```bash
php artisan vendor:publish --provider="\KDA\Laravel\MailHistory\ServiceProvider" --tag="views"
```

## Usage

```php
$laravel\MailHistory = new KDA\Laravel\MailHistory();
echo $laravel\MailHistory->echoPhrase('Hello, KDA!');
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](SECURITY.md) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdtk2)
- [All Contributors](CONTRIBUTORS.md)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
Please see [License File](LICENSE.KDA.md) for more information.
