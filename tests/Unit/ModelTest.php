<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\MailHistory\Models\MailLog;
use Faker\Factory;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use KDA\Laravel\MailHistory\Listeners\EmailListener;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function model_created()
  {
    $o = MailLog::factory()->create([]);
  }

    /** @test */
    function capture_email()
    {
  
      $faker = Factory::create();
      $recipient = $faker->safeEmail();
      Mail::raw('Test e-mail text', function ($message) use ($recipient) {
        $message->to($recipient)
          ->subject('the email subject');
      });
      Mail::html('Test e-mail text', function ($message) use ($recipient) {
        $message->to($recipient)
          ->subject('the email subject');
      });
      $this->assertDatabaseCount('mail_logs',2);
    }
  /** @test */
  function events()
  {

    $faker = Factory::create();
    $recipient = $faker->safeEmail();
    Event::fake();
    Mail::raw('Test e-mail text', function ($message) use ($recipient) {
      $message->to($recipient)
        ->subject('the email subject');
    });

    Event::assertDispatched(MessageSent::class, function (MessageSent $event) {
      return true;
    });
    Event::assertListening(
      MessageSent::class,
      EmailListener::class
    );
  }
}
