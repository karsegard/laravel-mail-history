<?php

namespace KDA\Laravel\MailHistory\Database\Factories;

use KDA\Laravel\MailHistory\Models\MailLog;
use Illuminate\Database\Eloquent\Factories\Factory;

class MailLogFactory extends Factory
{
    protected $model = MailLog::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
